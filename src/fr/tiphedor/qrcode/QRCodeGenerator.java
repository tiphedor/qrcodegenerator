/*
 * This is a free software distributed under the terms of the GNU : GPL V3 license. Please refer to the LICENCE file.
 */

package fr.tiphedor.qrcode;

import com.google.zxing.common.reedsolomon.GenericGF;
import com.google.zxing.common.reedsolomon.ReedSolomonEncoder;

/**
 * <p>Handle the backend of the QRCode generation</p>
 *
 * @author tiphedor
 */
public class QRCodeGenerator {

    /**
     * String representation of the input text to convert
     */
    private String words;

    /**
     * Initialize the class (... logic for a constructor, right?)
     *
     * @param input
     */
	public QRCodeGenerator(String input) {
		this.words = input; 
	}

    /**
     * Converts the String represention of the QR Code into a proper matrix.
     * @param qrin
     * @return qrMatrix
     *
     * @todo Investigate on a better way to do this
     *
     */
    public boolean[][] makeQRArrayFromString(String qrin) {
		boolean[][] out = new boolean[21][21];

		// Premi�re colone, on place les 24 premiers modules. On monte
		
		for(int i = 0; i != 12; i++) {
			out[20 - i][20] = stringToBool(Character.toString(qrin.charAt(2 * i)));
 			out[20 - i][19] = stringToBool(Character.toString(qrin.charAt((2*i) + 1)));
		}
						
		// Deuxi�me colone, on place les modules 25 � 48. On descend
		
		for(int i = 0; i != 12; i++) {
			out[9 + i][18] = stringToBool(Character.toString(qrin.charAt((2 * i) + 24)));
 			out[9 + i][17] = stringToBool(Character.toString(qrin.charAt((2 * i) + 25)));
 		}
		
		// Troisi�me colonne; on place les modules 49 � 72. On monte
		
		for(int i = 0; i != 12; i++) {
			out[20 - i][16] = stringToBool(Character.toString(qrin.charAt((2 * i) + 48)));
 			out[20 - i][15] = stringToBool(Character.toString(qrin.charAt((2 * i) + 49)));
		}
				
		// Quatrieme colonne, on place les modules 73 � 97. On descend.
		
		for(int i = 0; i != 12; i++) {
			out[9 + i][14] = stringToBool(Character.toString(qrin.charAt((2 * i) + 72)));
 			out[9 + i][13] = stringToBool(Character.toString(qrin.charAt((2 * i) + 73)));
		}
		
		// Cinquieme colonne, on place les modules on monte
		
		for(int i = 0; i != 14; i++) {
			out[20 - i][12] = stringToBool(Character.toString(qrin.charAt((2 * i) + 96)));
 			out[20 - i][11] = stringToBool(Character.toString(qrin.charAt((2 * i) + 97)));
		}
		for(int i = 0; i != 6; i++) {
			out[5 - i][12] = stringToBool(Character.toString(qrin.charAt((2 * i) + 124)));
 			out[5 - i][11] = stringToBool(Character.toString(qrin.charAt((2 * i) + 125)));
		}

		// Sixieme colonne, on descend 
		
		for(int i = 0; i != 6; i++) {
			out[i][10] = stringToBool(Character.toString(qrin.charAt((2 * i) + 136)));
 			out[i][9] = stringToBool(Character.toString(qrin.charAt((2 * i) + 137)));
		}
		for(int i = 0; i != 14; i++) {
			out[7 + i][10] = stringToBool(Character.toString(qrin.charAt((2 * i) + 148)));
 			out[7 + i][9] = stringToBool(Character.toString(qrin.charAt((2 * i) + 149)));
		}
		
		// Huitieme colonne, on monte 
		
		for(int i = 0; i != 4; i++) {
			out[12 - i][8] = stringToBool(Character.toString(qrin.charAt((2 * i) + 176)));
 			out[12 - i][7] = stringToBool(Character.toString(qrin.charAt((2 * i) + 177)));
		}
		
		// Neuvieme colonne, on descend 
		
		for(int i = 0; i != 4; i++) {
			out[9 +  i][5] = stringToBool(Character.toString(qrin.charAt((2 * i) + 184)));
 			out[9 +  i][4] = stringToBool(Character.toString(qrin.charAt((2 * i) + 185)));
		}
		
		// 10e colonne, on monte 
		
		for(int i = 0; i != 4; i++) {
			out[12 - i][3] = stringToBool(Character.toString(qrin.charAt((2 * i) + 192)));
 			out[12 - i][2] = stringToBool(Character.toString(qrin.charAt((2 * i) + 193)));
		}
		
		// 11e et derniere (ENFIN) colonne, on descend 
		
		for(int i = 0; i != 4; i++) {
			out[9 +  i][1] = stringToBool(Character.toString(qrin.charAt((2 * i) + 200)));
 			out[9 +  i][0] = stringToBool(Character.toString(qrin.charAt((2 * i) + 201)));
		}
				
		return out;
		
	}

    /**
     * Convert the string representation of 1 and 0 into true and false. If anything but 1 or 0 is specified, returns 0.
     * @param stringBinary
     * @return binary
     */
	public static boolean stringToBool(String stringBinary) {
		  if (stringBinary.equals("1"))
		    return true;
		  if (stringBinary.equals("0"))
		    return false;
		  else
			  return false;
	}

    /**
     * Return the numeric representation of a character. This representation is specific to QR Codes
     *
     * @todo Investigate on a better way to do this
     *
     * @param characterInput
     * @return numericRepresentation
     */
	public int getCharNumeric(char characterInput) {
		switch(characterInput) {
			case '0':
				return 0;
			case '1':
				return 1;
			case '2':
				return 2;
			case '3':
				return 3;
			case '4':
				return 4;
			case '5':
				return 5;
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
			case 'A':
				return 10;
			case 'B':
				return 11;
			case 'C':
				return 12;
			case 'D':
				return 13;
			case 'E':
				return 14;
			case 'F':
				return 15;
			case 'G':
				return 16;
			case 'H':
				return 17;
			case 'I':
				return 18;
			case 'J':
				return 19;
			case 'K':
				return 20;
			case 'L':
				return 21;
			case 'M':
				return 22;
			case 'N':
				return 23;
			case 'O':
				return 24;
			case 'P':
				return 25;
			case 'Q':
				return 26;
			case 'R':
				return 27;
			case 'S':
				return 28;
			case 'T':
				return 29;
			case 'U':
				return 30;
			case 'V':
				return 31;
			case 'W':
				return 32;
			case 'X':
				return 33;
			case 'Y':
				return 34;
			case 'Z':
				return 35;
			case ' ':
				return 36;
			case '$':
				return 37;
			case '%':
				return 38;
			case '*':
				return 39;
			case '+':
				return 40;	
			case '-':
				return 41;
			case '.':
				return 42;
			case '/':
				return 43;
			case ':':
				return 44;
			default:
				return 0;


		}
	}

	/**
	 * Generate the binary string corresponding to the input text
	 *
	 * @return string
	 */
	public String makeQRBinaryString() {
		String output = "";
        String currentChar = "";
        String charNbre = "";

        // On précise le mode alphanumérique
        output = "0010";

        // On ajoute le nombre de caractères convertit en binaire, puis on s'asure que ce compte fasse 9bits

        charNbre = Integer.toBinaryString(this.words.length());
        while(charNbre.length() < 9) {
            charNbre = "0" + charNbre;
        }

        output += charNbre;

        // On ajoute les caractères

        if(this.words.length() % 2 == 0) { // nombre pair de caractères
            for(int i = 0; i != this.words.length(); i+=2) { // On ajoute les caractères 2 par 2 sous la forme binaire(45 * CodeCaractère1 + CodeCaractère2)
                currentChar = Integer.toBinaryString(45 * getCharNumeric(this.words.charAt(i)) + getCharNumeric(this.words.charAt(i + 1)));
                // On fait tenir ce nombre binaire sur 11 bits
                while(currentChar.length() < 11) {
                    currentChar = "0" + currentChar;
                }
                output += currentChar;
            }
        } else { //Nombre impair de caracètre
            for(int i = 0; i != this.words.length() - 1; i+=2) { // On procède comme pour un nombre pair, en ignorant le dernier caractère
                currentChar = Integer.toBinaryString(45 * getCharNumeric(this.words.charAt(i)) + getCharNumeric(this.words.charAt(i + 1)));
                // On fait tenir ce nombre binaire sur 11 bits
                while(currentChar.length() < 11) {
                    currentChar = "0" + currentChar;
                }
                output += currentChar;
            }
            // Ensuite, on ajoute le dernier caractère sous la forme d'un singuler (= CodeCaractère) de 6bits
            currentChar = Integer.toBinaryString(getCharNumeric(this.words.charAt(this.words.length() - 1)));
            while(currentChar.length() < 6) {
                currentChar = "0" + currentChar;
            }
            output += currentChar;
        }

        // On génère un code de type 1-M. Il doit contenir 128bits. Si c'est le cas : GO

        if(output.length() == 128) {
            return output;
        }

        // Sinon, on commence par ajouter le terminator : 1, 2 3 ou 4 zéros à droite de la chaîne

        for(int i = 0; i != 4; i++) {
            if(output.length() == 128) {
                return output;
            }
            output += "0";
        }

        // S'il n'y a toujours pas assez de bits, on commence par rendre la longueur de la chaîne multiple de 8

        while(output.length() % 8 != 0) {
            output += "0";
        }

        // On ajoute le masque 11101100 00010001 jusqu'à ce que le code soit complet

        while(output.length() < 128) {
            output += "11101100";
            if(output.length() == 128) {
                return output;
            }
            output += "00010001";
        }

        return output;

	}

    /**
     * Generate the Error correction codewords thanks to the Zxing external library.
     *
     * @todo auto convert int array to byte string
     *
     * @param dataBytes
     * @param numEcBytesInBlock
     * @return ecInt
     */
	public int[] generateECBytes(byte[] dataBytes, int numEcBytesInBlock) {
	    int numDataBytes = dataBytes.length;
	    int[] toEncode = new int[numDataBytes + numEcBytesInBlock];
	    for (int i = 0; i < numDataBytes; i++) {
	      toEncode[i] = dataBytes[i] & 0xFF;
	    }
	    new ReedSolomonEncoder(GenericGF.QR_CODE_FIELD_256).encode(toEncode, numEcBytesInBlock);

	    int[] ecInt = new int[numEcBytesInBlock];
	    
	    for (int i = 0; i < numEcBytesInBlock; i++) {
	    	ecInt[i] = (int) ((byte) toEncode[numDataBytes + i] & 0xFF) ;
	    }
	    return ecInt;
	}

    /**
     * Generate the base QR Matrix with mandatory patterns
     *
     * @todo 1 line function.
     *
     * @return qrBaseMatrix
     */
	public boolean[][] generateBaseQRMatrix() {
		boolean[][] qrMatrix = new boolean[21][21];
		
		// On place les finders patterns
		
		// Haut-gauche

        qrMatrix[0][0] = true;
		qrMatrix[0][1] = true;
		qrMatrix[0][2] = true;
		qrMatrix[0][3] = true;
		qrMatrix[0][4] = true;
		qrMatrix[0][5] = true;
		qrMatrix[0][6] = true;
		
		qrMatrix[1][0] = true;
		qrMatrix[1][6] = true;
		
		qrMatrix[2][0] = true;
		qrMatrix[2][2] = true;
		qrMatrix[2][3] = true;
		qrMatrix[2][4] = true;
		qrMatrix[2][6] = true;
		
		qrMatrix[3][0] = true;
		qrMatrix[3][2] = true;
		qrMatrix[3][3] = true;
		qrMatrix[3][4] = true;
		qrMatrix[3][6] = true;

		qrMatrix[4][0] = true;
		qrMatrix[4][2] = true;
		qrMatrix[4][3] = true;
		qrMatrix[4][4] = true;
		qrMatrix[4][6] = true;
		
		qrMatrix[5][0] = true;
		qrMatrix[5][6] = true;

		qrMatrix[6][0] = true;
		qrMatrix[6][1] = true;
		qrMatrix[6][2] = true;
		qrMatrix[6][3] = true;
		qrMatrix[6][4] = true;
		qrMatrix[6][5] = true;
		qrMatrix[6][6] = true;
		
		// bas-gauche
		
		qrMatrix[14][0] = true;
		qrMatrix[14][1] = true;
		qrMatrix[14][2] = true;
		qrMatrix[14][3] = true;
		qrMatrix[14][4] = true;
		qrMatrix[14][5] = true;
		qrMatrix[14][6] = true;
		
		qrMatrix[15][0] = true;
		qrMatrix[15][6] = true;
		
		qrMatrix[16][0] = true;
		qrMatrix[16][2] = true;
		qrMatrix[16][3] = true;
		qrMatrix[16][4] = true;
		qrMatrix[16][6] = true;
		
		qrMatrix[17][0] = true;
		qrMatrix[17][2] = true;
		qrMatrix[17][3] = true;
		qrMatrix[17][4] = true;
		qrMatrix[17][6] = true;

		qrMatrix[18][0] = true;
		qrMatrix[18][2] = true;
		qrMatrix[18][3] = true;
		qrMatrix[18][4] = true;
		qrMatrix[18][6] = true;
		
		qrMatrix[19][0] = true;
		qrMatrix[19][6] = true;

		qrMatrix[20][0] = true;
		qrMatrix[20][1] = true;
		qrMatrix[20][2] = true;
		qrMatrix[20][3] = true;
		qrMatrix[20][4] = true;
		qrMatrix[20][5] = true;
		qrMatrix[20][6] = true;
		
		
		// Haut-droite
		
		qrMatrix[0][14] = true;
		qrMatrix[0][15] = true;
		qrMatrix[0][16] = true;
		qrMatrix[0][17] = true;
		qrMatrix[0][18] = true;
		qrMatrix[0][19] = true;
		qrMatrix[0][20] = true;
		
		qrMatrix[1][14] = true;
		qrMatrix[1][20] = true;
		
		qrMatrix[2][14] = true;
		qrMatrix[2][16] = true;
		qrMatrix[2][17] = true;
		qrMatrix[2][18] = true;
		qrMatrix[2][20] = true;
		
		qrMatrix[3][14] = true;
		qrMatrix[3][16] = true;
		qrMatrix[3][17] = true;
		qrMatrix[3][18] = true;
		qrMatrix[3][20] = true;

		qrMatrix[4][14] = true;
		qrMatrix[4][16] = true;
		qrMatrix[4][17] = true;
		qrMatrix[4][18] = true;
		qrMatrix[4][20] = true;
		
		qrMatrix[5][14] = true;
		qrMatrix[5][20] = true;

		qrMatrix[6][14] = true;
		qrMatrix[6][15] = true;
		qrMatrix[6][16] = true;
		qrMatrix[6][17] = true;
		qrMatrix[6][18] = true;
		qrMatrix[6][19] = true;
		qrMatrix[6][20] = true;
		
		
		// Timing pattern 
		
		// Vertical
		
		qrMatrix[6][8] = true;
		qrMatrix[6][10] = true;
		qrMatrix[6][12] = true;
		
		// Horizontal
		
		qrMatrix[6][8] = true;
		qrMatrix[6][10] = true;
		qrMatrix[6][12] = true;
		
		// Vertical
		
		qrMatrix[8][6] = true;
		qrMatrix[10][6] = true;
		qrMatrix[12][6] = true;
		
		// Dark Module
		
		qrMatrix[13][8] = true;
		
		return qrMatrix;
	}

	/**
	 * Split a string in substring each specified number of chars
	 * @param s
	 * @param interval
	 * @return result
	 */
	public static String[] splitStringEvery(String s, int interval) {
	    int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
	    String[] result = new String[arrayLength];

	    int j = 0;
	    int lastIndex = result.length - 1;
	    for (int i = 0; i < lastIndex; i++) {
	        result[i] = s.substring(j, j + interval);
	        j += interval;
	    } 
	    result[lastIndex] = s.substring(j);

	    return result;
	}

    /**
     * Convert a string into a byte array
     *
     * @param input
     * @return byteArray
     */
	public byte[] makeByteArrayFromString(String input) {
		String[] strQR = splitStringEvery(input, 8);
		byte[] byteQR = new byte[16];
		for (int y = 0; y != 16; y++) {
			byteQR[y] = (byte) Integer.parseInt(strQR[y], 2);
		}
		
		return byteQR;
	}

    /**
     * Convert an int array into a string corresponding to the binary representation of each array's row
     * @param array
     * @return output
     */
	public String intArrayToBinaryString(int[] array) {
		String out = "";
		for(int i = 0; i != 10; i++) {
			String currentBit = "";
			currentBit = Integer.toBinaryString(array[i]);
			while(currentBit.length() != 8) {
				currentBit = "0" + currentBit;
			}
			out += currentBit;
		}
		return out;
	}

    /**
     * Apply the type3 mask on a QRCode boolean array and add the format inforation to it.
     *
     * @param in
     * @return output
     *
     * @todo detection of the best mask, "smooth" matrix browse
     *
     */
	public boolean[][] getMaskedQRData(boolean[][] in) {
	    boolean[][] out = new boolean[21][21];
		
	    for(int i = 0; i != 6; i++) {
    		for(int y = 0; y != 4; y++) {
    			if((y + 9) % 3 == 0) {
    				out[i][y + 9] = !in[i][y + 9];
    			} else {
    				out[i][y + 9] = in[i][y + 9];
    			}
       		}
	    }
		
	    for(int y = 0; y != 6; y++) {
   			for(int i = 0; i != 4; i++) {
    			if((y + 9) % 3 == 0) {
	    			out[i + 9][y] = !in[i + 9][y];
		   		} else {
					out[i + 9][y] = in[i + 9][y];
				}
            }
		}
		
		for(int y = 0; y != 14; y++) {
			for(int i = 0; i != 4; i++) {
				if((y + 7) % 3 == 0) {
					out[i + 9][y + 7] = !in[i + 9][y + 7];
				} else { 
					out[i + 9][y + 7] = in[i + 9][y + 7];
				}
			}
		}

		for(int i = 0; i != 2; i++) {
			for(int y = 0; y != 4; y++) {
				if((y + 9) % 3 == 0) {
					out[i + 7][y + 9] = !in[i + 7][y + 9];
				} else {
					out[i + 7][y + 9] = in[i + 7][y + 9];
				}
			}
		}

	    for(int y = 0; y != 12; y++) {
		    for(int i = 0; i != 8; i++) {
			    if((y + 9) % 3 == 0) {
				    out[i + 13][y + 9] = !in[i + 13][y + 9];
			    } else  {
    				out[i + 13][y + 9] = in[i + 13][y + 9];
	    		}
			}
	    }

        out[2][8] = true;
        out[3][8] = true;
        out[4][8] = true;
        out[5][8] = true;
        out[7][8] = true;

        out[8][0] = true;
        out[8][2] = true;
        out[8][3] = true;
        out[8][4] = true;
        out[8][5] = true;

        out[20][8] = true;
        out[18][8] = true;
        out[17][8] = true;
        out[16][8] = true;
        out[15][8] = true;

        out[8][18] = true;
        out[8][17] = true;
        out[8][16] = true;
        out[8][15] = true;
        out[8][14] = true;

		return out;
	}
}