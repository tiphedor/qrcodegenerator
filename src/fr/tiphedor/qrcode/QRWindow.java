/*
 * This is a free software distributed under the terms of the GNU : GPL V3 license. Please refer to the LICENCE file.
 */

package fr.tiphedor.qrcode;


import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Handle the graphical part of the program.
 *
 * @author tiphedor
 */
public class QRWindow extends JFrame {

    /**
     * Initialise the class and show the main window
     * @param args
     */
    public static void main(String[] args) {
        QRWindow fenetre = new QRWindow();
    }

    /**
     * Text field for the data to encode
     */
    private JTextField txtQRString;

    /**
     * Submit button
     */
    private JButton btnSendQREncoding;

    /**
     * Returns the value of the inputbox
     * @return inputBoxValue
     */
    public String getQRInput() {
        return txtQRString.getText();
    }

    /**
     * Draw the QR on the window
     */
    public void paintQRCode(boolean[][] mQrMatrix, boolean[][] mQrData) {
        int color[] = {0, 0, 0};
        this.getContentPane().removeAll();
        this.setContentPane(new Modules(mQrMatrix, mQrData, color));
        this.getContentPane().revalidate();
        this.getContentPane().repaint();
        displayForm();
    }

    /**
     * Shows the input form on screen
     */
    public void displayForm() {
        this.setLayout(null);
        txtQRString.setBounds(340, 5, 135, 25);
        btnSendQREncoding.setBounds(480, 5, 100, 25);
        this.getContentPane().add(btnSendQREncoding);
        this.getContentPane().add(txtQRString);
    }

    /**
     * Initialize the class and show the window filled by the QR-Code content
     */
	public QRWindow() {
		
		// Initialisation of the forms elements
		
        txtQRString = new JTextField(10);
        btnSendQREncoding = new JButton();
        btnSendQREncoding.setText("Encoder");
        btnSendQREncoding.setBounds(200,200,20,20);

        // Window init
        
        this.setTitle("G�n�rateur de QR Codes");
		this.setSize(600, 375);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        displayForm();
        this.setVisible(true);
        
        // Handle the clicks on the button
        
        btnSendQREncoding.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textToEncode = getQRInput();
                if(textToEncode.length() == 0) {
                    new JOptionPane().showMessageDialog(null, "Votre message ne peut �tre vide", "Erreur !", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if(textToEncode.length() > 20) {
                    new JOptionPane().showMessageDialog(null, "Votre message doit faire moins de 20 caract�res", "Erreur !", JOptionPane.ERROR_MESSAGE);
                    return;
                }
           
                textToEncode = textToEncode.toUpperCase();
                QRCodeGenerator qrGen = new QRCodeGenerator(textToEncode);
                String qrBaseString = qrGen.makeQRBinaryString();
                String qrFullString = qrBaseString + qrGen.intArrayToBinaryString(qrGen.generateECBytes(qrGen.makeByteArrayFromString(qrBaseString), 10));
                paintQRCode(qrGen.generateBaseQRMatrix(), qrGen.getMaskedQRData(qrGen.makeQRArrayFromString(qrFullString)));
            }
        });
	}


    /**
     * Handle the modules placement in matrix.
     *
     * @author tiphedor
     */
	public class Modules extends JPanel {

        /**
         * Base QR pattern
         */
		private boolean[][] qrMatrix;

        /**
         * Data matrix
         */
		private boolean[][] qrData;

        /**
         * Color options for matrix. Array is {RED, GREEN, BLUEE}, color are mapped with 8bits (0-255)
         */
        private int color[];

        /**
         * Initialize class with matrix values
         *
         * @param matrix
         * @param data
         */
        public Modules(boolean[][] matrix, boolean[][] data, int mColor[]) {
			this.qrMatrix = matrix;
			this.qrData = data;
            this.color = mColor;
		}

        /**
         * Loop in modules from both base matrix and data matrix and paint them
         * @param g
         */
		public void paintComponent(Graphics g) { 
			for(int i = 0; i != 21; i++) {
				for(int y = 0; y != 21; y++) {
					drawNewQrModule(y, i, g, this.qrMatrix[i][y] || this.qrData[i][y]);
				}
			}
		}
		
		/**
		 * Draw a QR Module at specified coordinates
		 * @param x
		 * @param y
		 * @param graphe
		 * @param color
		 */
		public void drawNewQrModule(int x, int y, Graphics graphe, boolean color) {
			if(color) {
				graphe.setColor(new Color(this.color[0], this.color[1], this.color[2]));
			} else {
				graphe.setColor(new Color(255,255,255));
			}
			graphe.fillRect(x*16, y*16, 16, 16);
		}
	}
}